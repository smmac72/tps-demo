#include "TPS_demoCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Animation/SkeletalMeshActor.h"
//#include "AnimGraphRuntime/Public/AnimNodes/AnimNode_RandomPlayer.h"
#include "TPS_demo/ComponentInventory.h"
#include "TPS_demo/Game/TPS_demoGameInstance.h"

ATPS_demoCharacter::ATPS_demoCharacter()
{
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	GetCharacterMovement()->bOrientRotationToMovement = true; 
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true);
	CameraBoom->TargetArmLength = 600.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false;
	
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false;

	InventoryComponent = CreateDefaultSubobject<UComponentInventory>(TEXT("InventoryComponent"));
	if (InventoryComponent)
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ATPS_demoCharacter::SwitchWeapon);
	InitialMaxSpeed = GetCharacterMovement()->MaxWalkSpeed;
	
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}
void ATPS_demoCharacter::BeginPlay()
{
	Super::BeginPlay();

	InitWeapon(EWeaponNames::WEAPON_PISTOL_DONATE);
	
	if (CursorMaterial)
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
}
void ATPS_demoCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	if (CurrentCursor) // cursor update
	{
		APlayerController* PlayerController = Cast<APlayerController>(GetController());
		if (PlayerController)
		{
			FHitResult TraceHitResult;
			PlayerController->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			const FVector CursorFV = TraceHitResult.ImpactNormal;
			const FRotator CursorR = CursorFV.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}
	
	MovementTick(DeltaSeconds);
	EquipTick(DeltaSeconds);
}

void ATPS_demoCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	InputComponent->BindAxis(TEXT("MoveForward"), this, &ATPS_demoCharacter::InputAxisVertical);
	InputComponent->BindAxis(TEXT("MoveRight"), this, &ATPS_demoCharacter::InputAxisHorizontal);
	InputComponent->BindAxis(TEXT("MoveWheel"), this, &ATPS_demoCharacter::ActivateCameraSlide);

	InputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &ATPS_demoCharacter::InputAttackPressed);
	InputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &ATPS_demoCharacter::InputAttackReleased);
	InputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &ATPS_demoCharacter::ReloadWeapon);

	InputComponent->BindAction(TEXT("EquipPistolEvent"), EInputEvent::IE_Pressed, this, &ATPS_demoCharacter::EquipPistol);
	InputComponent->BindAction(TEXT("EquipHeavyEvent"), EInputEvent::IE_Pressed, this, &ATPS_demoCharacter::EquipHeavy);
	InputComponent->BindAction(TEXT("PickWeaponEvent"), EInputEvent::IE_Pressed, this, &ATPS_demoCharacter::PickWeapon);
	InputComponent->BindAction(TEXT("DropWeaponEvent"), EInputEvent::IE_Pressed, this, &ATPS_demoCharacter::DropWeapon);
	
	InputComponent->BindAction(TEXT("MovementStyleAim"), EInputEvent::IE_Pressed, this, &ATPS_demoCharacter::SetMovementStyleAimEnabled);
	InputComponent->BindAction(TEXT("MovementStyleAim"), EInputEvent::IE_Released, this, &ATPS_demoCharacter::SetMovementStyleAimDisabled);
	InputComponent->BindAction(TEXT("MovementStyleSprint"), EInputEvent::IE_Pressed, this, &ATPS_demoCharacter::SetMovementStyleSprintEnabled);
	InputComponent->BindAction(TEXT("MovementStyleSprint"), EInputEvent::IE_Released, this, &ATPS_demoCharacter::SetMovementStyleSprintDisabled);
	InputComponent->BindAction(TEXT("MovementStyleWalk"), EInputEvent::IE_Pressed, this, &ATPS_demoCharacter::SetMovementStyleWalkEnabled);
	InputComponent->BindAction(TEXT("MovementStyleWalk"), EInputEvent::IE_Released, this, &ATPS_demoCharacter::SetMovementStyleWalkDisabled);
}

UDecalComponent* ATPS_demoCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}

void ATPS_demoCharacter::InputAxisVertical(float Value)
{
	AxisVertical = Value;
	IsWalkingBack = (Value == -1.f);
}

void ATPS_demoCharacter::InputAxisHorizontal(float Value)
{
	AxisHorizontal = Value;
}

void ATPS_demoCharacter::EquipPistol()
{
	const int8 OldIndex = CurrentWeaponIndex;
	FAdditionalWeaponInfo OldWeaponInfo;

	if (CurrentWeapon)
	{
		OldWeaponInfo = CurrentWeapon->WeaponInfo;
		if (CurrentWeapon->WeaponReloading)
			CurrentWeapon->CancelReload();
	}

	if (InventoryComponent)
		InventoryComponent->SwitchWeaponToIndex(0, OldIndex, OldWeaponInfo);

}

void ATPS_demoCharacter::EquipHeavy()
{
	const int8 OldIndex = CurrentWeaponIndex;
	FAdditionalWeaponInfo OldWeaponInfo;

	if (CurrentWeapon)
	{
		OldWeaponInfo = CurrentWeapon->WeaponInfo;
		if (CurrentWeapon->WeaponReloading)
			CurrentWeapon->CancelReload();
	}
	
	if (InventoryComponent)
		InventoryComponent->SwitchWeaponToIndex(1, OldIndex, OldWeaponInfo);
}

void ATPS_demoCharacter::PickWeapon()
{
	// todo later
}

void ATPS_demoCharacter::DropWeapon()
{
	if (GetCurrentWeapon()->WeaponSetting.WeaponType != EWeaponType::STATE_PISTOL)
	{
		USkeletalMeshComponent* WeaponMesh = GetCurrentWeapon()->SkeletalMeshWeapon;
		if (WeaponMesh)
		{
			FTransform WeaponDropTransform;
			WeaponDropTransform.SetLocation(WeaponMesh->GetComponentLocation());
			WeaponDropTransform.SetScale3D(WeaponMesh->GetComponentScale());
			WeaponDropTransform.SetRotation(FQuat(WeaponMesh->GetComponentRotation()));
			
			FActorSpawnParameters WeaponDropParam;
			WeaponDropParam.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			
			AWeaponDefault* WeaponDropMesh = GetWorld()->SpawnActor<AWeaponDefault>(GetCurrentWeapon()->WeaponSetting.WeaponClass, WeaponDropTransform, WeaponDropParam);
			
			WeaponMesh->SetVisibility(false);
			if (WeaponDropMesh)
			{
				WeaponDropMesh->SkeletalMeshWeapon->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
				WeaponDropMesh->SkeletalMeshWeapon->SetCollisionProfileName("PhysicsActor");
				WeaponDropMesh->SkeletalMeshWeapon->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Ignore);
				WeaponDropMesh->SkeletalMeshWeapon->SetSimulatePhysics(true);
				WeaponDropMesh->SkeletalMeshWeapon->AddForce(FVector(0, 0, -1) * 100);
				WeaponDropMesh->SetLifeSpan(69);
			}
			SwitchWeapon(EWeaponNames::WEAPON_PISTOL);
		}
	}
}

void ATPS_demoCharacter::InputAttackPressed()
{
	AttackCharacterEvent(true);
}

void ATPS_demoCharacter::InputAttackReleased()
{
	AttackCharacterEvent(false);
}

void ATPS_demoCharacter::SetMovementStyleAimEnabled()
{
	IsTransitioningToAim = true;
	if (GetCurrentWeapon()->WeaponSetting.WeaponType == EWeaponType::STATE_SNIPER)
	{
		SniperOldArmLength = GetCameraBoom()->TargetArmLength;
		GetCameraBoom()->TargetArmLength = CameraMaxHeight + 300;
	}
	ChangeMovementState();
}

void ATPS_demoCharacter::SetMovementStyleAimDisabled()
{
	IsTransitioningToAim = false;
	if (SniperOldArmLength != 0)
	{
		GetCameraBoom()->TargetArmLength = SniperOldArmLength;
		SniperOldArmLength = 0;
	}
	ChangeMovementState();
}

void ATPS_demoCharacter::MovementTick(float DeltaTime)
{
	AddMovementInput(FVector(1.f, 0.f, 0.f), AxisVertical);
	AddMovementInput(FVector(0.f, 1.f, 0.f), AxisHorizontal);

	if (MovementState == EMovementState::STATE_SPRINT)
	{
		const FVector RotationVector = FVector(AxisVertical,AxisHorizontal,0.0f);
		const FRotator Rotator = RotationVector.ToOrientationRotator();
		SetActorRotation((FQuat(Rotator)));
	}
	else
	{
		APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		if (PlayerController)
		{
			FHitResult ResultHit;
			PlayerController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);

			// вращашка, чтобы анимки работали при повороте камеры
			const float FindRotatorResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
			const float YawDifference = FindRotatorResultYaw - GetActorRotation().Yaw;
			SetActorRotation(FQuat(FRotator(0.0f, FindRotatorResultYaw, 0.0f)));
			if (GetVelocity() == FVector::ZeroVector)
			{
				if (YawDifference > 0.1)
				{
					TurnRight = true;
					TurnLeft = false;
				}
				else
					TurnRight = false;
				
				if (YawDifference < -0.1)
				{
					TurnRight = false;
					TurnLeft = true;
				}
				else
					TurnLeft = false;
			}
			else
			{
				TurnRight = false;
				TurnLeft = false;
			}
			
			// господи я все еще не знаю почему оно работает
			if (AxisVertical != 0 || AxisHorizontal != 0) // штука для изменения скорости в зависимости от векторов направления инпута и взгляда мышкой
			{
				FVector MoveVector = FVector(AxisVertical, AxisHorizontal, 0);
				FVector MouseDirectionAngle = FVector(ResultHit.Location.X - GetActorLocation().X, ResultHit.Location.Y - GetActorLocation().Y, 0);
				MoveVector.Normalize();
				MouseDirectionAngle.Normalize();

				CurrentMoveDirection = (MoveVector.Rotation() - MouseDirectionAngle.Rotation()).Yaw;
				if (CurrentMoveDirection > 180)
					CurrentMoveDirection = 360 - CurrentMoveDirection;
				else if (CurrentMoveDirection < -180)
					CurrentMoveDirection = -360 - CurrentMoveDirection;
				DirectionCoef = abs(CurrentMoveDirection);

				// костыль
				if (abs(CurrentMoveDirection) > 30 && InitialMaxSpeed == MovementInfo.SprintSpeed)
					InitialMaxSpeed = MovementInfo.RunSpeed;
			
				if (InitialMaxSpeed - DirectionCoef < 150)
					DirectionCoef = InitialMaxSpeed - 150;
				GetCharacterMovement()->MaxWalkSpeed = InitialMaxSpeed - DirectionCoef;
			}

			if (CurrentWeapon)
			{
				const FVector Displacement = FVector(0.f, 0.f, CurrentWeapon->ShootLocation->GetComponentLocation().Z);
				CurrentWeapon->ShootEndLocation = ResultHit.Location + Displacement;
			}
		}
	}
}

void ATPS_demoCharacter::EquipTick(float DeltaTime)
{
	if (IsEquipping)
	{
		EquipTimer -= DeltaTime;
		if (!bCanBeEquipped)
			if (EquipTimer <= InitialEquipTime / 2) // magic kostyl
				bCanBeEquipped = true;
		if (EquipTimer <= 0.f)
		{
			IsEquipping = false;
			EquipTimer = 0.f;
		}
	}
}

void ATPS_demoCharacter::SwitchWeapon(EWeaponNames NewWeapon)
{
	if (EquipTimer <= 0.f && !IsEquipping)
	{
		if (CurrentWeapon)
		{
			IsEquipping = true;
			if (IsAiming)
				EquipTimer = GetMesh()->GetAnimInstance()->Montage_Play(CurrentWeapon->WeaponSetting.AnimAimSwitch, 1, EMontagePlayReturnType::MontageLength, 0, false);
			else 
				EquipTimer = GetMesh()->GetAnimInstance()->Montage_Play(CurrentWeapon->WeaponSetting.AnimStandSwitch, 1, EMontagePlayReturnType::MontageLength, 0, false);
			InitialEquipTime = EquipTimer;
		}
		else
			InitialEquipTime = .1f;
		InitWeapon(NewWeapon);
	}
}


void ATPS_demoCharacter::CharacterUpdate()
{
	float NewSpeed = 0.f;
	switch (MovementState)
	{
		case EMovementState::STATE_AIM:
			NewSpeed = MovementInfo.AimSpeed;
			break;
		case EMovementState::STATE_AIMWALK:
			NewSpeed = MovementInfo.AimWalkSpeed;
			break;
		case EMovementState::STATE_WALK:
			NewSpeed = MovementInfo.WalkSpeed;
			break;
		case EMovementState::STATE_RUN:
			NewSpeed = MovementInfo.RunSpeed;
			break;
		case EMovementState::STATE_SPRINT:
			if (abs(CurrentMoveDirection < 30))
				NewSpeed = MovementInfo.SprintSpeed;
			else
				NewSpeed = MovementInfo.RunSpeed;
			break;
	}
	GetCharacterMovement()->MaxWalkSpeed = NewSpeed;
	InitialMaxSpeed = NewSpeed;
}

void ATPS_demoCharacter::ChangeMovementState()
{
	// upd: я понятия не имею будет ли оно работать мне надоело рисовать в тетради графы связующие эти состояния
	// upd2: вроде работает
	if (!IsReloading)
		IsAiming = IsTransitioningToAim;
	if (!IsWalking && !IsSprinting && !IsAiming)
		MovementState = EMovementState::STATE_RUN;
	else
	{
		if (IsSprinting)
		{
			IsWalking = false;
			IsAiming = false;
			MovementState = EMovementState::STATE_SPRINT;
		}
		if (IsWalking && IsAiming)
			MovementState = EMovementState::STATE_AIMWALK;
		else
		{
			if (IsWalking && !IsSprinting && !IsAiming)
				MovementState = EMovementState::STATE_WALK;
			else if (!IsWalking && IsAiming)
				MovementState = EMovementState::STATE_AIM;
		}
	}	
	CharacterUpdate();

	//Weapon state update
	AWeaponDefault* PlayerWeapon = GetCurrentWeapon();
	if (PlayerWeapon)
		PlayerWeapon->UpdateStateWeapon(MovementState);
}

void ATPS_demoCharacter::ReloadWeapon()
{
	if (CurrentWeapon)
	{
		if (!CurrentWeapon->WeaponSetting.IsReloadable)
			DropWeapon();
		else
		{
			if (!IsReloading && CurrentWeapon->WeaponInfo.CurrentRounds < CurrentWeapon->WeaponSetting.MaxRound)
			{
				IsReloading = true;
				CurrentWeapon->InitWeaponReload();
			}
		}
	}
}

void ATPS_demoCharacter::CameraSlide(float Value)
{
	if (SniperOldArmLength == 0)
	{
		if (!IsSliding)
		{
			const float InitialHeight = GetCameraBoom()->TargetArmLength;
			if (Value > 0)
			{
				if (InitialHeight + CameraHeightStep <= CameraMaxHeight)
				{
					SlideDirection = 1;
					CameraCallTimer();
				}
			}
			else if (Value < 0)
			{
				if (InitialHeight - CameraHeightStep >= CameraMinHeight)
				{
					SlideDirection = -1;
					CameraCallTimer();
				}
			}
		}
	}
}

void ATPS_demoCharacter::CameraSlideTick()
{
	const float CameraHeightIncrease = CameraHeightStepTick * SlideDirection;
	CameraCurrentDistance += CameraHeightIncrease;
	GetCameraBoom()->TargetArmLength += CameraHeightIncrease;
	if (abs(CameraCurrentDistance) >= abs(CameraNewHeight))
	{
		CameraCurrentDistance = 0;
		IsSliding = false;
		UKismetSystemLibrary::K2_ClearTimerHandle(GetWorld(), CameraTickTimer);
	}
}

void ATPS_demoCharacter::CameraCallTimer()
{
	CameraNewHeight = CameraHeightStep * SlideDirection;
	IsSliding = true;
	CameraTickTimer = UKismetSystemLibrary::K2_SetTimer(this, "CameraSlideTick", CameraTickTimerTime, true);
}

AWeaponDefault* ATPS_demoCharacter::GetCurrentWeapon() const
{
	return CurrentWeapon;
}

void ATPS_demoCharacter::InitWeapon(EWeaponNames NewWeapon) 
{
	AttackCharacterEvent(false); // костыль иногда крашит просто
	UTPS_demoGameInstance *MyGameInstance = Cast<UTPS_demoGameInstance>(GetWorld()->GetGameInstance());
	if (MyGameInstance)
	{
		if (bCanBeEquipped)
		{
			if (CurrentWeapon)
			{
				CurrentWeapon->Destroy();
				CurrentWeapon = nullptr;
			}
			
			bCanBeEquipped = false;
			
			FWeaponInfo WeaponInfo;
			MyGameInstance->GetWeaponInfoByEnum(NewWeapon, WeaponInfo);
			if (WeaponInfo.WeaponClass)
			{
				const FVector SpawnLocation = FVector(0);
				const FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* PlayerWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(WeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (PlayerWeapon)
				{
					const FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					PlayerWeapon->AttachToComponent(GetMesh(), Rule, FName("middle_01_r_WeaponSocket"));
					CurrentWeapon = PlayerWeapon;
					
					PlayerWeapon->WeaponSetting = WeaponInfo;
					//PlayerWeapon->WeaponInfo = PlayerWeapon;
					PlayerWeapon->UpdateStateWeapon(MovementState);
					
					SwitchWeapon(NewWeapon);

					if (InventoryComponent)
						CurrentWeaponIndex = InventoryComponent->GetWeaponIndexByType(NewWeapon);
				
					PlayerWeapon->OnWeaponReloadStart.AddDynamic(this, &ATPS_demoCharacter::WeaponReloadStart);
					PlayerWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATPS_demoCharacter::WeaponReloadEnd);
				}
			}
		}
	}
}

void ATPS_demoCharacter::TryReloadWeapon()
{
	if (CurrentWeapon)
	{
		if (CurrentWeapon->GetWeaponCurrentRounds() < CurrentWeapon->WeaponSetting.MaxRound)
			CurrentWeapon->InitWeaponReload();
	}
}

void ATPS_demoCharacter::WeaponReloadStart()
{
	if (!GetCurrentWeapon()->WeaponSetting.IsReloadable)
		DropWeapon();
	else
		WeaponReloadStart_BP();
}

void ATPS_demoCharacter::WeaponReloadEnd()
{
	WeaponReloadEnd_BP();
}

void ATPS_demoCharacter::WeaponReloadStart_BP_Implementation()
{
	// in BP
}

void ATPS_demoCharacter::WeaponReloadEnd_BP_Implementation()
{
	// in BP
}

void ATPS_demoCharacter::AttackCharacterEvent(bool bIsFiring)
{
	AWeaponDefault* PlayerWeapon = GetCurrentWeapon();
	if (PlayerWeapon)
	{
		if (!IsEquipping)
		{
			IsFiring = bIsFiring;
			if (PlayerWeapon)
				PlayerWeapon->SetWeaponStateFire(bIsFiring);
		}
	}
}
