#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TPS_demo/FuncLibrary/Types.h"
#include "TPS_demo/WeaponDefault.h"
#include "TPS_demoCharacter.generated.h"

DECLARE_DYNAMIC_DELEGATE(FOnWeaponAnimationFinished);

UCLASS(Blueprintable)
class ATPS_demoCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ATPS_demoCharacter();

	virtual void Tick(float DeltaSeconds) override;
	virtual void BeginPlay() override;

	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	// cursor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		FVector CursorSize = FVector(20.f, 40.f, 40.f);
	UFUNCTION(BlueprintCallable)
		UDecalComponent* GetCursorToWorld();

	// input
	UFUNCTION()
		void InputAxisVertical(float Value);
	UFUNCTION()
		void InputAxisHorizontal(float Value);
	UFUNCTION()
		void EquipPistol();
	UFUNCTION()
		void EquipHeavy();
	UFUNCTION()
		void PickWeapon();
	UFUNCTION()
		void DropWeapon();
	UFUNCTION()
		void InputAttackPressed();
	UFUNCTION()
		void InputAttackReleased();
	UFUNCTION()
		void ActivateCameraSlide(float Value) { CameraSlide(Value); }
	UFUNCTION()
		void SetMovementStyleAimEnabled();
	UFUNCTION()
		void SetMovementStyleAimDisabled();
	UFUNCTION()
		void SetMovementStyleSprintEnabled() { IsSprinting = true; ChangeMovementState(); }
	UFUNCTION()
		void SetMovementStyleSprintDisabled() { IsSprinting = false; ChangeMovementState(); }
	UFUNCTION()
		void SetMovementStyleWalkEnabled() { IsWalking = true; ChangeMovementState(); }
	UFUNCTION()
		void SetMovementStyleWalkDisabled() { IsWalking = false; ChangeMovementState(); }
	UFUNCTION()
		void MovementTick(float DeltaTime);
	UFUNCTION()
		void EquipTick(float DeltaTime);

	// update movement state
	UPROPERTY(BlueprintReadOnly)
		EMovementState MovementState = EMovementState::STATE_RUN;

	// movement states
	UPROPERTY(BlueprintReadOnly, Category = "Movement Style")
		float CurrentMoveDirection = 0.f;
	UPROPERTY(BlueprintReadWrite, Category = "Movement Style")
		bool IsWalking = false;
	UPROPERTY(BlueprintReadWrite, Category = "Movement Style")
		bool IsTransitioningToAim = false;
	UPROPERTY(BlueprintReadWrite, Category = "Movement Style")
		bool IsAiming = false;
	UPROPERTY(BlueprintReadWrite, Category = "Movement Style")
		bool IsRunning = true;
	UPROPERTY(BlueprintReadWrite, Category = "Movement Style")
		bool IsSprinting = false;
	UPROPERTY(BlueprintReadWrite, Category = "Movement Style")
		bool IsFiring = false;
	UPROPERTY(BlueprintReadWrite, Category = "Movement Style")
		bool IsReloading = false;
	UPROPERTY(BlueprintReadWrite, Category = "Movement Style")
		bool IsEquipping = false;
	UPROPERTY(BlueprintReadWrite, Category = "Movement Style")
		bool IsDead = false;
	UPROPERTY(BlueprintReadWrite, Category = "Movement Style")
		bool TurnLeft = false;
	UPROPERTY(BlueprintReadWrite, Category = "Movement Style")
		bool TurnRight = false;
	UPROPERTY(BlueprintReadWrite, Category = "Movement Style")
		bool IsFalling = true;
	UPROPERTY(BlueprintReadWrite, Category = "Movement Style")
		bool IsMoving = false;
	UPROPERTY(BlueprintReadWrite, Category = "Movement Style")
		bool IsWalkingBack = false;
	
	// camera slide
	UFUNCTION(BlueprintCallable)
		void CameraSlide(float Value);
	UFUNCTION()
		void CameraCallTimer();
	UFUNCTION()
		void CameraSlideTick();
	
	// camera slide
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = CameraSlide)
		float CameraMinHeight = 500.f;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = CameraSlide)
		float CameraMaxHeight = 1000.f;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = CameraSlide)
		float CameraHeightStep = 100.f;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = CameraSlide)
		float CameraHeightStepTick = 1.f;

	// weapon
	UFUNCTION(BlueprintCallable)
		AWeaponDefault* GetCurrentWeapon() const;
	UFUNCTION(BlueprintCallable)
		void InitWeapon(EWeaponNames StartWeapon);
	UFUNCTION()
		void SwitchWeapon(EWeaponNames NewWeapon);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		EWeaponNames StartWeapon;
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
		int32 CurrentWeaponIndex = 0;

	// equip
	UPROPERTY(BlueprintReadOnly)
		float EquipTimer = 0.f;
	FOnWeaponAnimationFinished OnWeaponAnimationFinished;

	// reload
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadStart_BP();
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadEnd_BP();

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	class UComponentInventory* InventoryComponent;

	// cursor
	UDecalComponent* CurrentCursor = nullptr;

	// movement axis
	float AxisVertical = 0.f;
	float AxisHorizontal = 0.f;

	// movement states
	UFUNCTION()
		void CharacterUpdate();
	UFUNCTION()
		void ChangeMovementState();
	FCharacterSpeed MovementInfo;
	float DirectionCoef = 0.f;
	float InitialMaxSpeed = 0.f;
	float SniperOldArmLength = 0;

	// camera slide
	FTimerHandle CameraTickTimer;
	bool IsSliding = false;
	float SlideDirection = 1;
	float CameraNewHeight = 0;
	float CameraTickTimerTime = .001;
	float CameraCurrentDistance = 0.f;

	// weapon
	AWeaponDefault* CurrentWeapon = nullptr;
	void AttackCharacterEvent(bool bIsFiring);
	bool bCanBeEquipped = true;
	float InitialEquipTime = 0.f;
	
	// reload
	UFUNCTION()
		void TryReloadWeapon();
	UFUNCTION()
		void WeaponReloadStart();
	UFUNCTION()
		void WeaponReloadEnd();
	UFUNCTION()
		void ReloadWeapon();
};

