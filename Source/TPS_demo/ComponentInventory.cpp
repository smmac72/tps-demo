// Fill out your copyright notice in the Description page of Project Settings.


#include "ComponentInventory.h"

#include "Game/TPS_demoGameInstance.h"
#include "Character/TPS_demoCharacter.h"

// Sets default values for this component's properties
UComponentInventory::UComponentInventory()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UComponentInventory::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
	for (int8 i = 0; i < WeaponSlots.Max(); i++)
	{
		UTPS_demoGameInstance *MyGameInstance = Cast<UTPS_demoGameInstance>(GetWorld()->GetGameInstance());
		if (MyGameInstance)
		{
			if (WeaponSlots[i].Weapon.WeaponName != EWeaponNames::WEAPON_NONE)
			{
				FWeaponInfo WeaponInfo;
				if (MyGameInstance->GetWeaponInfoByEnum(WeaponSlots[i].Weapon.WeaponName, WeaponInfo))
				{
					WeaponSlots[i].Weapon.WeaponInfo.CurrentRounds = WeaponInfo.MaxRound;
					//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, FString::Printf(TEXT("added weapon id %d max ammo %d"), i, WeaponSlots[i].Weapon.WeaponInfo.CurrentRounds));
				}
				else
					GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("No weapon on the slot"));
			}
		}
	}

	if (WeaponSlots.IsValidIndex(0))
		OnSwitchWeapon.Broadcast(WeaponSlots[0].Weapon.WeaponName);
	
}


// Called every frame
void UComponentInventory::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

bool UComponentInventory::SwitchWeaponToIndex(int32 NewIndex, int32 OldIndex, FAdditionalWeaponInfo OldWeaponInfo)
{
	if (NewIndex == OldIndex)
		return false;
	const EWeaponNames NewWeapon = WeaponSlots[NewIndex].Weapon.WeaponName;
	if (NewWeapon != EWeaponNames::WEAPON_NONE)
	{
		SetAdditionalWeaponInfo(OldIndex, OldWeaponInfo);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, FString::Printf(TEXT("Switching to index %d"), NewIndex));
		OnSwitchWeapon.Broadcast(NewWeapon);
		return true;
	}
	return false;
}

FAdditionalWeaponInfo UComponentInventory::GetAdditionalWeaponInfo(int32 WeaponIndex)
{
	return WeaponSlots[WeaponIndex].Weapon.WeaponInfo;
}

void UComponentInventory::SetAdditionalWeaponInfo(int32 WeaponIndex, FAdditionalWeaponInfo NewWeaponInfo)
{
	// index check
	WeaponSlots[WeaponIndex].Weapon.WeaponInfo = NewWeaponInfo;
}

int32 UComponentInventory::GetWeaponIndexByType(EWeaponNames WeaponName)
{
	// -1 empty, 0 pistol, 1 rifle/shotgun, 2 special (sniper, launchers)
	if (WeaponName == EWeaponNames::WEAPON_NONE)
		return -1;
	if (WeaponName == EWeaponNames::WEAPON_PISTOL || WeaponName == EWeaponNames::WEAPON_PISTOL_DONATE)
		return 0;
	if (WeaponName == EWeaponNames::WEAPON_RIFLE || WeaponName == EWeaponNames::WEAPON_RIFLE_DONATE
			|| WeaponName == EWeaponNames::WEAPON_SHOTGUN || WeaponName == EWeaponNames::WEAPON_SHOTGUN_DONATE)
		return 1;
	return 2;
}

