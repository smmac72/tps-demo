// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "FuncLibrary/Types.h"
#include "ComponentInventory.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnSwitchWeapon, EWeaponNames, WeaponName);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TPS_DEMO_API UComponentInventory : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UComponentInventory();
	
	FOnSwitchWeapon OnSwitchWeapon;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapons")
		TArray<FSlotsInventory> WeaponSlots;

	bool SwitchWeaponToIndex(int32 NewIndex, int32 OldIndex, FAdditionalWeaponInfo OldWeaponInfo);

	FAdditionalWeaponInfo GetAdditionalWeaponInfo(int32 WeaponIndex);
	void SetAdditionalWeaponInfo(int32 WeaponIndex, FAdditionalWeaponInfo NewWeaponInfo);

	static int32 GetWeaponIndexByType(EWeaponNames WeaponName);
};
