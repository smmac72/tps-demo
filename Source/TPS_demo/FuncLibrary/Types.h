// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"
#include "Engine/StaticMeshActor.h"
#include "Types.generated.h"


UENUM(BlueprintType)
enum class EWeaponNames : uint8
{
	WEAPON_NONE UMETA(DisplayName = "None"),
	WEAPON_PISTOL UMETA(DisplayName = "Pistol"),
	WEAPON_PISTOL_DONATE UMETA(DisplayName = "Pistol Donate"),
	WEAPON_RIFLE UMETA(DisplayName = "Rifle"),
	WEAPON_RIFLE_DONATE UMETA(DisplayName = "Rifle Donate"),
	WEAPON_SHOTGUN UMETA(DisplayName = "Shotgun"),
	WEAPON_SHOTGUN_DONATE UMETA(DisplayName = "Shotgun Donate"),
	WEAPON_SNIPER UMETA(DisplayName = "Sniper Rifle"),
	WEAPON_SNIPER_DONATE UMETA(DisplayName = "Sniper Rifle Donate"),
	WEAPON_GLAUNCHER UMETA(DisplayName = "Grenade Launcher"),
	WEAPON_GLAUNCHER_DONATE UMETA(DisplayName = "Grenade Launcher Donate"),
	WEAPON_RLAUNCHER UMETA(DisplayName = "Rocket Launcher"),
	WEAPON_RLAUNCHER_DONATE UMETA(DisplayName = "Rocket Launcher Donate")
};

UENUM(BlueprintType)
enum class EMovementState : uint8
{
	STATE_AIM UMETA(DisplayName = "Aim State"),
	STATE_AIMWALK UMETA(DisplayName = "Walk with Aiming State"),
	STATE_WALK UMETA(DisplayName = "Walk State"),
	STATE_RUN UMETA(DisplayName = "Run State"),
	STATE_SPRINT UMETA(DisplayName = "Sprint State")
};

UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	STATE_DEFAULT UMETA(DisplayName = "Default"),
	STATE_PISTOL UMETA(DisplayName = "Pistol"),
	STATE_RIFLE UMETA(DisplayName = "Rifle"),
	STATE_SHOTGUN UMETA(DisplayName = "Shotgun"),
	STATE_SNIPER UMETA(DisplayName = "Sniper Rifle"),
	STATE_GRENADELAUNCHER UMETA(DisplayName = "Grenade Launcher"),
	STATE_ROCKETLAUNCHER UMETA(DisplayName = "Rocket Launcher")
};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement Type")
	float AimSpeed = 378.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement Type")
	float AimWalkSpeed = 126.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement Type")
	float WalkSpeed = 150.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement Type")
	float RunSpeed = 450.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement Type")
	float SprintSpeed = 600.f;
};

USTRUCT(BlueprintType)
struct FDecals : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	TArray<UMaterialInterface*> Decals;
};

USTRUCT(BlueprintType)
struct FProjectileInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		TSubclassOf<class AProjectileDefault> Projectile = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		float ProjectileDamage = 20.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		float ProjectileLifeTime = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		float ProjectileInitSpeed = 2000.0f;

	//material to decal on hit
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	TMap<TEnumAsByte<EPhysicalSurface>, FDecals> HitDecals;
	// mesh for translucent decals
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		UStaticMesh* TranslucentActorDecalMesh;
	// decal lifetime
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		float DecalLifetime;
	//Sound when hit
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		TMap<TEnumAsByte<EPhysicalSurface>, USoundBase*> HitSFX;
	//fx when hit check by surface
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
		TMap<TEnumAsByte<EPhysicalSurface>, UParticleSystem*> HitFXs;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ExplosiveProjectile")
		bool IsExplodable = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ExplosiveProjectile")
		bool IsExplodingOnImpact = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ExplosiveProjectile")
		float TimeToExplode = 2.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ExplosiveProjectile")
		UParticleSystem* ExplosionFX = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ExplosiveProjectile")
		USoundBase* ExplosionSound = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ExplosiveProjectile")
		float ProjectileMaxRadiusDamage = 200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ExplosiveProjectile")
		float ExplosionMaxDamage = 40.0f;
};

USTRUCT(BlueprintType)
struct FWeaponDispersion
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Aim_StateDispersionAimMax = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Aim_StateDispersionAimMin = 0.3f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Aim_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Aim_StateDispersionReduction = .3f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float AimWalk_StateDispersionAimMax = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float AimWalk_StateDispersionAimMin = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float AimWalk_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float AimWalk_StateDispersionReduction = 0.4f;
};

USTRUCT(BlueprintType)
struct FWeaponInfo : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Class")
		TSubclassOf<class AWeaponDefault> WeaponClass = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Default")
		EWeaponType WeaponType;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		float RateOfFire = 0.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		float ReloadTime = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		float ReloadModifier = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		bool ReloadByOne = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		bool IsReloadable = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		int32 MaxRound = 10;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		int32 NumberProjectileByShot = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		FWeaponDispersion DispersionWeapon;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile ")
		FProjectileInfo ProjectileSetting;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace ")
		float WeaponDamage = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace ")
		float DistanceTrace = 2000.0f;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitEffect ")
		UDecalComponent* DecalOnHit = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Anim ")
		UAnimSequence* WeaponFire = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Anim ")
		UAnimSequence* WeaponReload = nullptr;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim ")
		UAnimMontage* AnimFire = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim ")
		UAnimMontage* AnimStandSwitch = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim ")
		UAnimMontage* AnimAimSwitch = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim ")
		UAnimMontage* AnimStandReload = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim ")
		UAnimMontage* AnimAimReload = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Mesh ")
		UStaticMesh* ShellBullets = nullptr;
		
};

USTRUCT(BlueprintType)
struct FAdditionalWeaponInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Stats")
	int32 CurrentRounds = 10;
};

USTRUCT(BlueprintType)
struct FWeaponInventory
{
	GENERATED_BODY()
	
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Slot")
		//int32 Index = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Slot")
		EWeaponNames WeaponName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Slot")
		FAdditionalWeaponInfo WeaponInfo;
};

USTRUCT(BlueprintType)
struct FAmmoInventory
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo Slot")
		int32 CurrentRounds = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo Slot")
		int32 MaxRounds = 0;
};

USTRUCT(BlueprintType)
struct FSlotsInventory
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Slots")
	FWeaponInventory Weapon;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Slots")
	FAmmoInventory Ammo;
};

UCLASS()
class TPS_DEMO_API UTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
};