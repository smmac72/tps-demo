// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS_demoGameInstance.h"

#include "TPS_demo/WeaponDefault.h"

bool UTPS_demoGameInstance::GetWeaponInfoByName(FName WeaponName, FWeaponInfo& OutInfo)
{
	FWeaponInfo* WeaponInfoRow = WeaponInfoTable->FindRow<FWeaponInfo>(WeaponName, "", false);
	OutInfo = *WeaponInfoRow;
	if (WeaponInfoRow)
		return true;
	else return false;
}

bool UTPS_demoGameInstance::GetWeaponInfoByEnum(EWeaponNames WeaponName, FWeaponInfo& OutInfo)
{
	switch (WeaponName)
	{
	case EWeaponNames::WEAPON_PISTOL:
		if (GetWeaponInfoByName("Weapon_Pistol_Black", OutInfo))
			return true;
		break;
	case EWeaponNames::WEAPON_PISTOL_DONATE:
		if (GetWeaponInfoByName("Weapon_Pistol_Silver", OutInfo))
			return true;
		break;
	case EWeaponNames::WEAPON_RIFLE:
		if (GetWeaponInfoByName("Weapon_Rifle_Black", OutInfo))
			return true;
		break;
	case EWeaponNames::WEAPON_RIFLE_DONATE:
		if (GetWeaponInfoByName("Weapon_Rifle_Silver", OutInfo))
			return true;
		break;
	case EWeaponNames::WEAPON_SHOTGUN:
		if (GetWeaponInfoByName("Weapon_Shotgun_Black", OutInfo))
			return true;
		break;
	case EWeaponNames::WEAPON_SHOTGUN_DONATE:
		if (GetWeaponInfoByName("Weapon_Shotgun_Silver", OutInfo))
			return true;
		break;
	case EWeaponNames::WEAPON_SNIPER:
		if (GetWeaponInfoByName("Weapon_SniperRifle_Black", OutInfo))
			return true;
		break;
	case EWeaponNames::WEAPON_SNIPER_DONATE:
		if (GetWeaponInfoByName("Weapon_SniperRifle_Silver", OutInfo))
			return true;
		break;
	case EWeaponNames::WEAPON_GLAUNCHER:
		if (GetWeaponInfoByName("Weapon_GrenadeLauncher_Black", OutInfo))
			return true;
		break;
	case EWeaponNames::WEAPON_GLAUNCHER_DONATE:
		if (GetWeaponInfoByName("Weapon_GrenadeLauncher_Silver", OutInfo))
			return true;
		break;
	case EWeaponNames::WEAPON_RLAUNCHER:
		if (GetWeaponInfoByName("Weapon_RocketLauncher_Black", OutInfo))
			return true;
		break;
	case EWeaponNames::WEAPON_RLAUNCHER_DONATE:
		if (GetWeaponInfoByName("Weapon_RocketLauncher_Silver", OutInfo))
			return true;
		break;
	case EWeaponNames::WEAPON_NONE:
		return false;
	}
	GetWeaponInfoByName("Weapon_Pistol_Black", OutInfo);
	return false;
}