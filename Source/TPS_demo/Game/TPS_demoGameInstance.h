// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "TPS_demo/FuncLibrary/Types.h"
#include "Engine/DataTable.h"
#include "TPS_demoGameInstance.generated.h"

UCLASS()
class TPS_DEMO_API UTPS_demoGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Settings")
		UDataTable* WeaponInfoTable = nullptr;
	UFUNCTION(BlueprintCallable)
		bool GetWeaponInfoByName(FName WeaponName, FWeaponInfo& OutInfo);
	UFUNCTION(BlueprintCallable)
		bool GetWeaponInfoByEnum(EWeaponNames WeaponName, FWeaponInfo& OutInfo);

};

