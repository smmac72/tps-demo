#include "TPS_demoGameMode.h"
#include "TPS_demoPlayerController.h"
#include "TPS_demo/Character/TPS_demoCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATPS_demoGameMode::ATPS_demoGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATPS_demoPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/BP_Character"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}