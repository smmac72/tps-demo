#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TPS_demoGameMode.generated.h"

UCLASS(minimalapi)
class ATPS_demoGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATPS_demoGameMode();
};



