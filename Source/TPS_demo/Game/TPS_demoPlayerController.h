#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TPS_demoPlayerController.generated.h"

UCLASS()
class ATPS_demoPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ATPS_demoPlayerController();

protected:
	uint32 bMoveToMouseCursor : 1;

	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;

	void OnResetVR();

	void MoveToMouseCursor();

	void MoveToTouchLocation(const ETouchIndex::Type FingerIndex, const FVector Location);
	
	void SetNewMoveDestination(const FVector DestLocation);

	void OnSetDestinationPressed();
	void OnSetDestinationReleased();
};


