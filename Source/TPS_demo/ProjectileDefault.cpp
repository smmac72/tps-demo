// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault.h"

#include "Components/DecalComponent.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"
#define DEBUGMESSAGE(x, ...) if(GEngine){GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, FString::Printf(TEXT(x), __VA_ARGS__));}

// Sets default values
AProjectileDefault::AProjectileDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BulletCollisionSphere = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Sphere"));
	BulletCollisionSphere->SetSphereRadius(16.f);
	BulletCollisionSphere->bReturnMaterialOnMove = true;//hit event return physMaterial
	BulletCollisionSphere->SetCanEverAffectNavigation(false);//collision not affect navigation (P keybord on editor)
	RootComponent = BulletCollisionSphere;
	
	BulletMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet Projectile Mesh"));
	BulletMesh->SetupAttachment(RootComponent);
	BulletMesh->SetCanEverAffectNavigation(false);

	BulletFX = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Bullet FX"));
	BulletFX->SetupAttachment(RootComponent);

	BulletProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("Bullet ProjectileMovement"));
	BulletProjectileMovement->UpdatedComponent = RootComponent;
	BulletProjectileMovement->InitialSpeed = 1.f;
	BulletProjectileMovement->MaxSpeed = 0.f;

	BulletProjectileMovement->bRotationFollowsVelocity = true;
	BulletProjectileMovement->bShouldBounce = true;
}

// Called when the game starts or when spawned
void AProjectileDefault::BeginPlay()
{
	Super::BeginPlay();

	BulletCollisionSphere->OnComponentHit.AddDynamic(this, &AProjectileDefault::BulletCollisionSphereHit);
	BulletCollisionSphere->OnComponentBeginOverlap.AddDynamic(this, &AProjectileDefault::BulletCollisionSphereBeginOverlap);
	BulletCollisionSphere->OnComponentEndOverlap.AddDynamic(this, &AProjectileDefault::BulletCollisionSphereEndOverlap);
}

// Called every frame
void AProjectileDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimerExplosion(DeltaTime);
}

void AProjectileDefault::InitProjectile(FProjectileInfo InitParam)
{
	BulletProjectileMovement->InitialSpeed = InitParam.ProjectileInitSpeed;
	BulletProjectileMovement->MaxSpeed = InitParam.ProjectileInitSpeed;
	this->SetLifeSpan(InitParam.ProjectileLifeTime);

	ProjectileSetting = InitParam;
}

void AProjectileDefault::BulletCollisionSphereHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (OtherActor && Hit.PhysMaterial.IsValid())
	{
		EPhysicalSurface SurfaceType = UGameplayStatics::GetSurfaceType(Hit);

		if (ProjectileSetting.DecalLifetime > 0)
		{
			if (ProjectileSetting.HitDecals.Contains(SurfaceType))
			{
				auto MaterialArray = ProjectileSetting.HitDecals[SurfaceType].Decals;
				auto ImpactMaterial = MaterialArray[FMath::RandRange(0, MaterialArray.Num() - 1)];
				if (ImpactMaterial && OtherComp)
				{
					if (OtherComp->GetMaterial(0)->GetBaseMaterial()->BlendMode == EBlendMode::BLEND_Translucent) // господи ну и костыль
					{
						FTransform TranslucentTransform;
						TranslucentTransform.SetLocation(Hit.ImpactPoint);
						TranslucentTransform.SetScale3D(FVector(.01, .25, .25));
						TranslucentTransform.SetRotation(FQuat(UKismetMathLibrary::FindLookAtRotation(Hit.TraceEnd, Hit.TraceStart)));
					
						FActorSpawnParameters TranslucentParam;
						TranslucentParam.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
						//TranslucentParam.Owner = this;
					
						AStaticMeshActor* TranslucentMesh = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), TranslucentTransform, TranslucentParam);

						if (TranslucentMesh && TranslucentMesh->GetStaticMeshComponent())
						{
							TranslucentMesh->SetActorTickEnabled(false);
							TranslucentMesh->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
							TranslucentMesh->GetStaticMeshComponent()->Mobility = EComponentMobility::Movable;
							TranslucentMesh->SetLifeSpan(ProjectileSetting.DecalLifetime);
							TranslucentMesh->GetStaticMeshComponent()->SetStaticMesh(ProjectileSetting.TranslucentActorDecalMesh);
							TranslucentMesh->GetStaticMeshComponent()->SetMaterial(0, ImpactMaterial);
						}
					}
					else
					{
						UDecalComponent* Decal = UGameplayStatics::SpawnDecalAttached(ImpactMaterial, FVector(20.0f), OtherComp, NAME_None, Hit.ImpactPoint, Hit.ImpactNormal.Rotation(),EAttachLocation::KeepWorldPosition,10.0f);
						if (Decal)
							Decal->SetLifeSpan(ProjectileSetting.DecalLifetime);
					}
				}
			}
		}
		if (ProjectileSetting.HitFXs.Contains(SurfaceType))
		{
			UParticleSystem* ImpactParticle = ProjectileSetting.HitFXs[SurfaceType];
			if (ImpactParticle)
				UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactParticle, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(1.0f)));
		}
			
		if (ProjectileSetting.HitSFX.Contains(SurfaceType))
		{
			USoundBase* ImpactSound = ProjectileSetting.HitSFX[SurfaceType];
			if (ImpactSound)
				UGameplayStatics::PlaySoundAtLocation(GetWorld(), ImpactSound, Hit.ImpactPoint);
		}
	
	}
	if (!ProjectileSetting.IsExplodable)
		UGameplayStatics::ApplyDamage(OtherActor, ProjectileSetting.ProjectileDamage, GetInstigatorController(), this, NULL);
	ImpactProjectile();	
}

void AProjectileDefault::BulletCollisionSphereBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
}

void AProjectileDefault::BulletCollisionSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
}

void AProjectileDefault::ImpactProjectile()
{
	if (ProjectileSetting.IsExplodingOnImpact)
		Explode();
	else if (!ProjectileSetting.IsExplodable)
		this->Destroy();
	else
		ExplosionTimerEnabled = true;
}

void AProjectileDefault::TimerExplosion(float DeltaTime)
{
	if (ExplosionTimerEnabled)
	{
		if (ExplosionTimer > ProjectileSetting.TimeToExplode)
			Explode();
		else
			ExplosionTimer += DeltaTime;
	}
}

void AProjectileDefault::Explode()
{
	ExplosionTimerEnabled = false;
	ExplosionTimer = 0.f;
	if (ProjectileSetting.ExplosionFX)
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(),ProjectileSetting.ExplosionFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
	if (ProjectileSetting.ExplosionSound)
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.ExplosionSound, GetActorLocation());

	
	const TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		ProjectileSetting.ExplosionMaxDamage,
		ProjectileSetting.ExplosionMaxDamage * 0.2f,
		GetActorLocation(),
		ProjectileSetting.ProjectileMaxRadiusDamage * 0.5,
		ProjectileSetting.ProjectileMaxRadiusDamage,
		5,
		NULL, IgnoredActor,nullptr,nullptr);
	
	//DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMaxRadiusDamage * 0.5, 100, FColor::Red, true, 3);
	//DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMaxRadiusDamage, 100, FColor::Orange, true, 3);
	
	this->Destroy();
}


