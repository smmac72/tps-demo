#include "WeaponDefault.h"
#include "DrawDebugHelpers.h"
#include "Components/DecalComponent.h"
#include "GameFramework/Character.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Math/UnrealMathUtility.h"

#define DEBUGMESSAGE(x, ...) if(GEngine){GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Red, FString::Printf(TEXT(x), __VA_ARGS__));}

// Sets default values
AWeaponDefault::AWeaponDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();
	WeaponInit();

}

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FireTick(DeltaTime);
	ReloadTick(DeltaTime);
	DispersionTick(DeltaTime);
}

void AWeaponDefault::FireTick(float DeltaTime)
{
	FireTimer -= DeltaTime;
	if (GetWeaponCurrentRounds() > 0)
	{
		if (WeaponFiring && FireTimer < 0.f)
		{
			if (WeaponReloading) // для дробовика шняга, чтобы стреляло когда угодно
				FinishReload();
			Fire();
		}
	}
	else if (!WeaponReloading)
		InitWeaponReload();
}

void AWeaponDefault::ReloadTick(float DeltaTime)
{
	if (WeaponReloading)
	{
		if (ReloadTimer <= 0.f)
		{
			if (WeaponSetting.ReloadByOne && WeaponInfo.CurrentRounds < WeaponSetting.MaxRound)
				MidReload();
			else
				FinishReload();
		}
		else
			ReloadTimer -= DeltaTime;
	}
}

void AWeaponDefault::DispersionTick(float DeltaTime)
{
	if (!WeaponReloading)
	{
		if (!WeaponFiring)
		{
			if (ShouldReduceDispersion)
				CurrentDispersion = CurrentDispersion - CurrentDispersionReduction;
			else
				CurrentDispersion = CurrentDispersion + CurrentDispersionReduction;
		}				

		if (CurrentDispersion < CurrentDispersionMin)
			CurrentDispersion = CurrentDispersionMin;
		else if (CurrentDispersion > CurrentDispersionMax)
			CurrentDispersion = CurrentDispersionMax;
	}
	if (ShowDebug)
		UE_LOG(LogTemp, Warning, TEXT("Dispersion: MAX = %f. MIN = %f. Current = %f"), CurrentDispersionMax, CurrentDispersionMin, CurrentDispersion);
}

void AWeaponDefault::WeaponInit()
{
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
		SkeletalMeshWeapon->DestroyComponent(true);

	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
		StaticMeshWeapon->DestroyComponent();
	
	UpdateStateWeapon(EMovementState::STATE_RUN);
}

void AWeaponDefault::SetWeaponStateFire(bool bIsFire)
{
	if (CheckWeaponCanFire())
		WeaponFiring = bIsFire;
	else
		WeaponFiring = false;
		//FireTimer = 0.01f;
}

bool AWeaponDefault::CheckWeaponCanFire() const
{
	return !BlockFire; // todo вдруг еще чет появится
}

FProjectileInfo AWeaponDefault::GetProjectile() const
{	
	return WeaponSetting.ProjectileSetting;
}

void AWeaponDefault::DropShells() const
{
	if (WeaponSetting.ShellBullets)
	{
		if (SkeletalMeshWeapon)
		{
			FTransform AmmoTransform;
			AmmoTransform.SetLocation(SkeletalMeshWeapon->GetSocketLocation("AmmoEject"));
			AmmoTransform.SetScale3D(FVector(1.5));
			AmmoTransform.SetRotation(FQuat(SkeletalMeshWeapon->GetSocketRotation("AmmoEject")));

			FActorSpawnParameters AmmoParam;
			AmmoParam.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			//AmmoParam.Owner = this;
					
			AStaticMeshActor* AmmoMesh = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), AmmoTransform, AmmoParam);

			if (AmmoMesh && AmmoMesh->GetStaticMeshComponent())
			{
				AmmoMesh->SetActorTickEnabled(false);
				AmmoMesh->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
				AmmoMesh->GetStaticMeshComponent()->SetCollisionProfileName("PhysicsActor");
				AmmoMesh->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Ignore);
				
				AmmoMesh->GetStaticMeshComponent()->Mobility = EComponentMobility::Movable;
				AmmoMesh->SetLifeSpan(5);
				AmmoMesh->GetStaticMeshComponent()->SetStaticMesh(WeaponSetting.ShellBullets);
				AmmoMesh->GetStaticMeshComponent()->SetSimulatePhysics(true);
				AmmoMesh->GetStaticMeshComponent()->AddForceAtLocation(FVector(0, FMath::RandRange(0.2f, 1.5f), FMath::RandRange(0.2f, 1.5f)) * 1000, SkeletalMeshWeapon->GetSocketLocation("AmmoEject"));
			}
		}
	}
}

void AWeaponDefault::Fire()
{
	FireTimer = WeaponSetting.RateOfFire;
	WeaponInfo.CurrentRounds--;
	
	ChangeDispersionByShot();
	DropShells();

	if (SkeletalMeshWeapon && WeaponSetting.WeaponFire) // анимка стрельбы у оружия
		SkeletalMeshWeapon->PlayAnimation(WeaponSetting.WeaponFire, false);

	auto PlayerAnimInstance = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0)->GetMesh()->GetAnimInstance();
	if (PlayerAnimInstance)
		if (WeaponSetting.AnimFire) // анимка стрельбы у персонажа, костыль потому что аним бп творит херню
			PlayerAnimInstance->Montage_Play(WeaponSetting.AnimFire, 1, EMontagePlayReturnType::MontageLength, 0, true);

	if (ShootLocation)
	{
		int8 NumberProjectile = GetNumberProjectileByShot();
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotation;
		FProjectileInfo ProjectileInfo;
		ProjectileInfo = GetProjectile();
		FVector EndLocation;
		
		for (int8 i = 0; i < NumberProjectile; i++)
		{
			EndLocation = GetFireEndLocation(); 

			FVector Dir = EndLocation - SpawnLocation;
			Dir.Normalize();
			
			FMatrix myMatrix(Dir, FVector(0, 1, 0), FVector(0, 0, 1), FVector(1, 0, 0));
			SpawnRotation = myMatrix.Rotator();

			if (ProjectileInfo.Projectile)
			{
				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				//SpawnParams.Owner = GetOwner();
				//SpawnParams.Instigator = GetInstigator();

				AProjectileDefault* myProjectile = Cast<AProjectileDefault>(GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myProjectile)
					myProjectile->InitProjectile(WeaponSetting.ProjectileSetting);
			}
			else
			{
				FHitResult HitResult;
				TArray<AActor*> IgnoreActors;
				if (ShowDebug)
					UKismetSystemLibrary::LineTraceSingle(GetWorld(), SpawnLocation, EndLocation * WeaponSetting.DistanceTrace,
					ETraceTypeQuery::TraceTypeQuery4, false, IgnoreActors, EDrawDebugTrace::ForDuration,
					HitResult, true);
				else
					UKismetSystemLibrary::LineTraceSingle(GetWorld(), SpawnLocation, EndLocation * WeaponSetting.DistanceTrace,
					ETraceTypeQuery::TraceTypeQuery4, false, IgnoreActors, EDrawDebugTrace::None,
					HitResult, true);

				if (HitResult.GetActor() && HitResult.PhysMaterial.IsValid())
				{
					EPhysicalSurface SurfaceType = UGameplayStatics::GetSurfaceType(HitResult);

					if (WeaponSetting.ProjectileSetting.HitDecals.Contains(SurfaceType))
					{
						auto MaterialArray = WeaponSetting.ProjectileSetting.HitDecals[SurfaceType].Decals;
						auto ImpactMaterial = MaterialArray[FMath::RandRange(0, MaterialArray.Num() - 1)];
						if (ImpactMaterial && HitResult.GetComponent())
						{
							if (HitResult.GetComponent()->GetMaterial(0)->GetBaseMaterial()->BlendMode == EBlendMode::BLEND_Translucent)
							{
								FTransform TranslucentTransform;
								TranslucentTransform.SetLocation(HitResult.ImpactPoint);
								TranslucentTransform.SetScale3D(FVector(.01, .25, .25));
								TranslucentTransform.SetRotation(HitResult.ImpactNormal.ToOrientationQuat());
				
								FActorSpawnParameters TranslucentParam;
								TranslucentParam.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
								//TranslucentParam.Owner = this;
				
								AStaticMeshActor* TranslucentMesh = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), TranslucentTransform, TranslucentParam);

								if (TranslucentMesh && TranslucentMesh->GetStaticMeshComponent())
								{
									TranslucentMesh->SetActorTickEnabled(false);
									TranslucentMesh->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
									TranslucentMesh->GetStaticMeshComponent()->Mobility = EComponentMobility::Movable;
									TranslucentMesh->SetLifeSpan(WeaponSetting.ProjectileSetting.DecalLifetime);
									TranslucentMesh->GetStaticMeshComponent()->SetStaticMesh(WeaponSetting.ProjectileSetting.TranslucentActorDecalMesh);
									TranslucentMesh->GetStaticMeshComponent()->SetMaterial(0, ImpactMaterial);
								}
							}
							else
							{
								UDecalComponent* Decal = UGameplayStatics::SpawnDecalAttached(ImpactMaterial, FVector(20.0f), HitResult.GetComponent(), NAME_None, HitResult.ImpactPoint, HitResult.ImpactNormal.Rotation(),EAttachLocation::KeepWorldPosition,10.0f);
								if (Decal)
									Decal->SetLifeSpan(WeaponSetting.ProjectileSetting.DecalLifetime);
							}
						}
					}
					if (WeaponSetting.ProjectileSetting.HitFXs.Contains(SurfaceType))
					{
						UParticleSystem* ImpactParticle = WeaponSetting.ProjectileSetting.HitFXs[SurfaceType];
						if (ImpactParticle)
							UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactParticle, FTransform(HitResult.ImpactNormal.Rotation(), HitResult.ImpactPoint, FVector(1.0f)));
					}
					if (WeaponSetting.ProjectileSetting.HitSFX.Contains(SurfaceType))
					{
						USoundBase* ImpactSound = WeaponSetting.ProjectileSetting.HitSFX[SurfaceType];
						if (ImpactSound)
							UGameplayStatics::PlaySoundAtLocation(GetWorld(), ImpactSound, HitResult.ImpactPoint);
					}
	
				}
				UGameplayStatics::ApplyDamage(HitResult.GetActor(), WeaponSetting.ProjectileSetting.ProjectileDamage, GetInstigatorController(), this, NULL);
			}
		}
	}
}

void AWeaponDefault::UpdateStateWeapon(EMovementState NewMovementState)
{
	BlockFire = false;

	switch (NewMovementState)
	{
	case EMovementState::STATE_AIM:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Aim_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
		
	case EMovementState::STATE_AIMWALK:
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.AimWalk_StateDispersionAimRecoil;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Aim_StateDispersionReduction;
		break;
		
	case EMovementState::STATE_SPRINT:
		BlockFire = true;
		SetWeaponStateFire(false);
		break;
	default:
		break;
	}
}

void AWeaponDefault::ChangeDispersionByShot()
{
	CurrentDispersion = CurrentDispersion + CurrentDispersionRecoil;
}

float AWeaponDefault::GetCurrentDispersion() const
{
	return CurrentDispersion;
}

FVector AWeaponDefault::ApplyDispersionToShoot(FVector DirectionShoot) const
{		
	return FMath::VRandCone(DirectionShoot, GetCurrentDispersion() * PI / 180.f);
}

FVector AWeaponDefault::GetFireEndLocation() const
{
	FVector EndLocation;

	const FVector tmpV = (ShootLocation->GetComponentLocation() - ShootEndLocation);

	if (tmpV.Size() > SizeVectorToChangeShootDirectionLogic)
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot((ShootLocation->GetComponentLocation() - ShootEndLocation).GetSafeNormal()) * -20000.0f;
		if (ShowDebug)
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), (ShootEndLocation - ShootLocation->GetComponentLocation()), WeaponSetting.DistanceTrace, GetCurrentDispersion()* PI / 180.f, GetCurrentDispersion() * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
	}	
	else
	{
		EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector()) * 20000.0f;
		if (ShowDebug)
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetForwardVector(), WeaponSetting.DistanceTrace, GetCurrentDispersion()* PI / 180.f, GetCurrentDispersion()* PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
	}
/*
	if (ShowDebug)
	{
		//direction weapon look
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * 500.0f, FColor::Cyan, false, 5.f, (uint8)'\000', 0.5f);
		//direction projectile must fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootEndLocation, FColor::Red, false, 5.f, (uint8)'\000', 0.5f);
		//Direction Projectile Current fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), EndLocation, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);

		DrawDebugSphere(GetWorld(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector()*SizeVectorToChangeShootDirectionLogic, 10.f, 8, FColor::Red, false, 4.0f);
	}*/
	return EndLocation;
}

int8 AWeaponDefault::GetNumberProjectileByShot() const
{
	return WeaponSetting.NumberProjectileByShot;
}

int32 AWeaponDefault::GetWeaponCurrentRounds()
{
	return WeaponInfo.CurrentRounds;
}

void AWeaponDefault::InitWeaponReload()
{
	WeaponReloading = true;
	if (!WeaponSetting.ReloadByOne)
	{
		if (WeaponInfo.CurrentRounds > 0)
			AmmoInChamberBeforeReload = true;
		WeaponInfo.CurrentRounds = 0;
	}
	ReloadTimer = WeaponSetting.ReloadTime;
	
	OnWeaponReloadStart.Broadcast();
}

void AWeaponDefault::CancelReload()
{
	WeaponReloading = false;
}

void AWeaponDefault::MidReload()
{
	WeaponInfo.CurrentRounds++;
	ReloadTimer = WeaponSetting.ReloadTime;

	if (WeaponInfo.CurrentRounds == WeaponSetting.MaxRound)
		FinishReload();
	else
		OnWeaponReloadStart.Broadcast();
}

void AWeaponDefault::FinishReload()
{
	WeaponReloading = false;
	if (!WeaponSetting.ReloadByOne)
		WeaponInfo.CurrentRounds = WeaponSetting.MaxRound + AmmoInChamberBeforeReload;
	AmmoInChamberBeforeReload = false;
	
	OnWeaponReloadEnd.Broadcast();
}
