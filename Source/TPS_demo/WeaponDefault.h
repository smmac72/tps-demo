#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"

#include "FuncLibrary/Types.h"
#include "ProjectileDefault.h"
#include "WeaponDefault.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWeaponReloadStart);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnWeaponReloadEnd);

UCLASS()
class TPS_DEMO_API AWeaponDefault : public AActor
{
	GENERATED_BODY()
	
	public:	
	AWeaponDefault();

	FOnWeaponReloadStart OnWeaponReloadStart;
	FOnWeaponReloadEnd OnWeaponReloadEnd;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UStaticMeshComponent* StaticMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UArrowComponent* ShootLocation = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
		FWeaponInfo WeaponSetting;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
		FAdditionalWeaponInfo WeaponInfo;

	protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	public:	
	// Tick func
	virtual void Tick(float DeltaTime) override;

	void FireTick(float DeltaTime);
	void ReloadTick(float DeltaTime);
	void DispersionTick(float DeltaTime);

	void WeaponInit();

	UFUNCTION(BlueprintCallable)
		void SetWeaponStateFire(bool bIsFire);
	UFUNCTION(BlueprintCallable)
		bool CheckWeaponCanFire() const;
	UFUNCTION(BlueprintCallable)
		int32 GetWeaponCurrentRounds();
	UFUNCTION()
		void UpdateStateWeapon(EMovementState NewMovementState);
	UFUNCTION()
		void InitWeaponReload();
	UFUNCTION()
		void CancelReload();
	
	FProjectileInfo GetProjectile() const;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		bool ShowDebug = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		float SizeVectorToChangeShootDirectionLogic = 100.0f;
	UPROPERTY(BlueprintReadOnly, Category = "FireLogic")
		bool WeaponFiring = false;
	UPROPERTY(BlueprintReadOnly, Category = "FireLogic")
		bool WeaponReloading = false;
	UPROPERTY(BlueprintReadOnly)
		FVector ShootEndLocation = FVector(0);
	
private:
	void Fire();
	void ChangeDispersionByShot();
	void DropShells() const;
	FVector ApplyDispersionToShoot(FVector DirectionShoot) const;
	FVector ShootLocationEnd = FVector(0);
	FVector GetFireEndLocation() const;
	int8 GetNumberProjectileByShot() const;
	float GetCurrentDispersion() const;
	
	//Timers'flags
	float FireTimer = 0.f;
	float ReloadTimer = 0.0f;
	
	//flags
	bool BlockFire = false;
	
	//Dispersion
	bool ShouldReduceDispersion = false;
	float CurrentDispersion = 0.0f;
	float CurrentDispersionMax = 1.0f;
	float CurrentDispersionMin = 0.1f;
	float CurrentDispersionRecoil = 0.1f;
	float CurrentDispersionReduction = 0.1f;

	// reload kostyl
	bool AmmoInChamberBeforeReload = false;

	// reload phases
	void MidReload();
	void FinishReload();
};